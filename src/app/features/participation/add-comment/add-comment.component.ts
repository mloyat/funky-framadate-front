import { Component, Input, OnInit } from '@angular/core';

import { PollService } from '../../../core/services/poll.service';

@Component({
	selector: 'app-add-comment',
	templateUrl: './add-comment.component.html',
	styleUrls: ['./add-comment.component.scss'],
})
export class AddCommentComponent implements OnInit {
	@Input() pseudo: string;
	public newComment: string;

	constructor(private pollService: PollService) {}

	ngOnInit(): void {}

	public saveComment(): void {
		this.pollService.addComment(this.newComment);
	}
}
