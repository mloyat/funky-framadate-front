import { Component, OnInit } from '@angular/core';
import { PollService } from '../../../core/services/poll.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ApiService } from '../../../core/services/api.service';
import { FormGroup } from '@angular/forms';
import { Poll } from '../../../core/models/poll.model';

@Component({
	selector: 'app-admin-consultation',
	templateUrl: './consultation.component.html',
	styleUrls: ['./consultation.component.scss'],
})
export class AdminConsultationComponent implements OnInit {
	private admin_key: string;
	public form: FormGroup;
	public poll: any;

	constructor(
		private pollService: PollService,
		private apiService: ApiService,
		private _Activatedroute: ActivatedRoute,
		private router: Router
	) {
		// http://localhost:4200/#/administration/key/7106Y3a5yfX10Z400fR670I1Bld0R4df7bEZ5fY6bOw5010t7XC36d4H16L2157jK4gb7JS311J735
		this.poll = this.pollService._poll.getValue();
		this.form = this.pollService.form;
	}

	ngOnInit(): void {
		this._Activatedroute.paramMap.subscribe((params: ParamMap) => {
			this.admin_key = params.get('admin_key');
			if (!this.admin_key) {
				this.router.navigate(['page-not-found']);
			}
			this.apiService.getPollByAdminKey(this.admin_key).then(
				(res) => {
					this.pollService.updateCurrentPoll(res.poll);
					this.form = this.pollService.form;
					this.poll = this.pollService._poll.getValue();
					console.log('formulaire patché', this.pollService.form, this.pollService.poll);
				},
				(err) => {
					if (!this.admin_key) {
						this.router.navigate(['page-not-found']);
					}
				}
			);
		});
	}
}
