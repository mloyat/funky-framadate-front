import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { PollService } from '../../../../../core/services/poll.service';
import { DOCUMENT } from '@angular/common';
import { ConfirmationService } from 'primeng/api';
import { Router } from '@angular/router';
import { environment } from '../../../../../../environments/environment';
import { Title } from '@angular/platform-browser';

@Component({
	selector: 'app-step-one',
	templateUrl: './step-one.component.html',
	styleUrls: ['./step-one.component.scss'],
})
export class StepOneComponent implements OnInit {
	@Input()
	step_max: any;
	@Input()
	form: FormGroup;
	public environment = environment;

	constructor(
		public pollService: PollService,
		@Inject(DOCUMENT) private document: any,
		private router: Router,
		private titleService: Title
	) {
		this.step_max = this.pollService.step_max;
		this.pollService.step_current = 1;
	}

	ngOnInit(): void {}

	leave(event: any) {
		console.log('event', event);
		if (event) {
			event.preventDefault();
		}
		const selector = '#title';
		const firstField = this.document.querySelector(selector);
		if (firstField) {
			firstField.focus();
		}
		this.router.navigate(['/']);
	}
}
