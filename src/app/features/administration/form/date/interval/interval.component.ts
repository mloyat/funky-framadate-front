import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../../../../../environments/environment';
import { DateUtilitiesService } from '../../../../../core/services/date.utilities.service';
import { FormArray, FormGroup } from '@angular/forms';
import { ToastService } from '../../../../../core/services/toast.service';
import { DateChoice } from '../../../../../../../mocks/old-stuff/config/defaultConfigs';

@Component({
	selector: 'app-date-interval',
	templateUrl: './interval.component.html',
	styleUrls: ['./interval.component.scss'],
})
export class IntervalComponent implements OnInit {
	@Input()
	public form: FormGroup;
	showDateInterval = true;
	intervalDays: any;
	intervalDaysDefault = environment.interval_days_default;
	startDateInterval: Date;
	startDateIntervalString: string;
	endDateInterval: Date;
	endDateIntervalString: string;
	dateList: DateChoice[]; // sets of dateChoices as strings, config to set identical time for dateChoices in a special dateChoices poll
	dateCalendarEnum: Date[] = [new Date('02/09/2021')];
	natural_lang_interval: string;

	constructor(public dateUtilities: DateUtilitiesService, private toastService: ToastService) {}

	ngOnInit(): void {
		this.form.get('startDateInterval').valueChanges.subscribe((val) => {
			console.log('val', val);
			if (typeof val === typeof String) {
				this.startDateIntervalString = val;
				this.countDays();
			} else {
				this.startDateInterval = val;
			}
		});
		this.form.get('endDateInterval').valueChanges.subscribe((val) => {
			console.log('val', val);
			if (typeof val === typeof String) {
				this.endDateIntervalString = val;
				this.countDays();
			}
			// this.endDateInterval = val;
			// this.countDays();
		});

		this.setDefaultDatesForInterval();
	}

	countDays(): void {
		// this.cd.detectChanges();
		this.intervalDays = this.dateUtilities.countDays(
			this.startDateInterval,
			this.endDateInterval
			// this.DateUtilitiesService.parseInputDateToDateObject(this.startDateIntervalString),
			// this.DateUtilitiesService.parseInputDateToDateObject(this.endDateIntervalString)
		);
		console.log('this.intervalDays ', this.intervalDays);
	}

	/**
	 * add all the dates between the start and end dates in the interval section
	 */
	addIntervalOfDates(): void {
		const newIntervalArray = this.dateUtilities.getDatesInRange(
			this.dateUtilities.parseInputDateToDateObject(this.startDateInterval),
			this.dateUtilities.parseInputDateToDateObject(this.endDateInterval),
			1
		);

		const converted = [];
		newIntervalArray.forEach((element) => {
			converted.push({
				literal: element.literal,
				date_object: element.date_object,
				timeList: [],
			});
		});
		this.dateList = [...new Set(converted)];
		// add only dates that are not already present with a Set of unique items
		console.log('this.dateChoices', this.dateList);
		this.showDateInterval = false;

		this.form.patchValue({ choices: this.dateList });

		this.toastService.display(`les dates ont été ajoutées aux réponses possibles.`);
	}

	get dateChoices() {
		return this.form.get('dateChoices') as FormArray;
	}

	/**
	 * default interval of dates proposed is from today to 7 dateChoices more
	 */
	setDefaultDatesForInterval(): void {
		if (this.form) {
			const dateCurrent = new Date();
			// const dateJson = dateCurrent.toISOString();
			this.startDateInterval = dateCurrent;
			this.endDateInterval = this.dateUtilities.addDaysToDate(this.intervalDaysDefault, dateCurrent);

			this.form.patchValue({
				startDateInterval: this.startDateInterval,
				endDateInterval: this.endDateInterval,
			});
			this.dateCalendarEnum = [
				dateCurrent,
				this.dateUtilities.addDaysToDate(this.intervalDaysDefault, dateCurrent),
			];
			this.countDays();
		}
	}

	addNaturalInstruction() {
		this.toastService.display(`TODO addNaturalInstruction`);
		console.log('TODO', this.natural_lang_interval);
	}
}
