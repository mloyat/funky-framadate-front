import { ChangeDetectorRef, Component, Inject, Input, OnInit } from '@angular/core';
import { LanguageService } from '../../../../core/services/language.service';
import { StorageService } from '../../../../core/services/storage.service';
import { DOCUMENT } from '@angular/common';
import { PollService } from '../../../../core/services/poll.service';
import { environment } from '../../../../../environments/environment';

@Component({
	selector: 'app-language-selector',
	templateUrl: './language-selector.component.html',
	styleUrls: ['./language-selector.component.scss'],
})
export class LanguageSelectorComponent implements OnInit {
	@Input() idSuffix: string = '';
	public currentLang: string;
	public availableLanguages: any = ['FR', 'EN', 'ES'];
	language: string;
	language_to_apply: string;
	display_lang_dialog: boolean = false;
	display_dynamic_langs: boolean = false;

	availableLanguagesStatic: any = [
		{ name: 'Arabic', value: 'ar' },
		{ name: 'German', value: 'de' },
		{ name: 'Français', value: 'fr' },
		{ name: 'Breton', value: 'br' },
		{ name: 'Catalan', value: 'ca' },
		{ name: 'Greek', value: 'el' },
		{ name: 'Espéranto', value: 'eo' },
		{ name: 'Español', value: 'es' },
		{ name: 'English', value: 'en' },
		{ name: 'Galician', value: 'gl' },
		{ name: 'Hungarian', value: 'hu' },
		{ name: 'Italiano', value: 'it' },
		{ name: '日本語', value: 'ja' },
		{ name: 'Nederlans', value: 'nl' },
		{ name: 'Occitan', value: 'oc' },
		{ name: 'Swedish', value: 'sv' },
	];

	constructor(
		private languageService: LanguageService,
		private storageService: StorageService,
		public pollService: PollService,
		private cd: ChangeDetectorRef,
		@Inject(DOCUMENT) private document: any
	) {}

	ngOnInit(): void {
		this.availableLanguages = this.languageService.getAvailableLanguages();

		if (this.storageService.language && this.availableLanguages.indexOf(this.storageService.language) !== -1) {
			this.languageService.setLanguage(this.storageService.language);
			this.currentLang = this.storageService.language;
		}
		this.currentLang = this.languageService.getLangage();
		this.language_to_apply = '' + this.currentLang;
	}

	setLang(newlang: string = environment.defaultLanguage): void {
		this.currentLang = newlang;
		this.languageService.setLanguage(newlang);
		this.storageService.language = this.currentLang;
		this.pollService.updateTitle();
	}

	closeModalAndFocusOnButtonToOpen() {
		this.display_lang_dialog = false;
		this.cd.detectChanges();
		let buttonClose = this.document.querySelector('#lang_button_popup');
		if (buttonClose) {
			buttonClose.focus();
		}
	}

	nextLang(): void {
		const index = this.availableLanguages.indexOf(this.currentLang);

		if (index !== -1) {
			// on passe au language suivant si il existe, sinon on revient au numéro 0

			if (this.availableLanguages[index + 1]) {
				this.currentLang = this.availableLanguages[index + 1];
			} else {
				this.currentLang = this.availableLanguages[0];
			}
			this.setLang();
		}
	}

	applyLangAndClosePopup() {
		this.setLang(this.language_to_apply);
		this.display_lang_dialog = false;
		this.cd.detectChanges();
		let elem = this.document.querySelector('#lang_button_popup');
		if (elem) {
			elem.focus();
		}
	}

	openDialogLang() {
		this.display_lang_dialog = true;
		this.cd.detectChanges();
		let elem = this.document.querySelector('#close_dialog');
		if (elem) {
			elem.focus();
		}
	}
}
