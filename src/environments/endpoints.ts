export const backendApiUrlsInDev = {
	// local: 'http://tktest.lan/api/v1',
	// remote: 'http://tktest.lan/api/v1',
	local: 'http://localhost:8000/api/v1', // the docker port is this one
	// local: 'http://www.tk.lan/index.php/api/v1',
	// local: 'https://framadate-api.cipherbliss.com/api/v1',
	// remote: 'http://localhost:8000/api/v1',
	// remote: 'http://www.tk.lan/index.php/api/v1',
	remote: 'https://framadate-api.cipherbliss.com/api/v1',
};
export const apiV1 = {
	// baseHref: 'http://localhost:8000/api/v1', // local "symfony serve" live server
	baseHref: 'http://www.tk.lan/index.php/api/v1', // local apache2 server
	// baseHref: 'http://tktest.lan/api/v1',
	// baseHref: 'https://framadate-api.cipherbliss.com/api/v1', // demo preprod
	api_new_poll: '/poll/',
	api_get_poll: '/poll/{id}',
	api_new_vote_stack: '/vote-stack',
	'api_test-mail-poll': '/api/v1/poll/mail/test-mail-poll/{emailChoice}',
	'app.swagger': '/api/doc.json',
};

export const endpoints = {
	versionToUse: 'apiV1',
	version: {
		apiV1,
	},
	baseHref: backendApiUrlsInDev.local,
	endpoints: {
		polls: {
			name: '/poll',
			choices: {
				name: '/choices',
			},
			comments: {
				name: '/comment/poll',
			},
			slugs: {
				name: '/slugs',
			},
			answers: {
				name: '/answers',
			},
		},
		users: {
			find_my_polls: '/api/v1/poll/owner/:email/',
			name: '/users',
			polls: {
				name: '/polls',
				sendEmail: {
					name: '/send-email',
				},
			},
		},
	},
};
