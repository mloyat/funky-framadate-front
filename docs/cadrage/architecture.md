# Architecture de Framadate
* Documentation de référence dans le dossier "doc" de ce dépot.
* Frontend généré avec Angular CLI
* chaque commit sur ce dépot passe les contrôles de format de Husky avant de pouvoir être envoyé.
* Doc sur l'api du nouveau backend à utiliser pour les appels REST du front funky : https://framagit.org/tykayn/date-poll-api/wikis/home
    toujours se référer à la sortie de la commande de symfony `bin/console debug:router` du dépot back. 

ancien backend en PHP sans framework ici pour mémoire : https://framagit.org/framasoft/framadate/framadate

# Schéma de base de données du backend
https://framagit.org/framasoft/framadate/funky-framadate-front/-/wikis/uploads/9d9a82cc6d7c6efea073b64d3667dc9b/framadate-api.png

png créé avec sqlfairy https://www.cipherbliss.com/exporter-une-visualisation-de-son-schma-sql/
